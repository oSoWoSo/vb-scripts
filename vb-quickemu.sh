#!/usr/bin/env bash
# This is only used for remove STDERR output...
#
exec 6>&2 ; exec 2> /dev/null
##################
source easybashgui
LIB_CHECK="$(type "easybashgui" 2> /dev/null )"
[ ${#LIB_CHECK} -eq 0 ] && echo -e "\n\n\n\nYou need to copy \"easybashgui\" in your path ( e.g.: \"cp easybashgui_X.X.X /usr/local/bin/\" )...\n:(\n\n\n\n" 1>&2 && exit 1
# ... "x" is the function name...
#
version="0.1 dev"
progname=${progname:="${0##*/}"}

# default menu
default_menu(){
	x=menu
		{
		QUESTION="$question"
		x="menu -w 650 -h 550"
		${x} "$polozky"
		answer="$(cat "${dir_tmp}/${file_tmp}" )"
		}
}
# default list
default_list(){
	x=list
		{
		QUESTION="$question"
		x="list -w 650 -h 550"
		list "$polozky"
		answer="$(cat "${dir_tmp}/${file_tmp}" )"
		}
}
# main menu
main_menu(){
	question='Main menu'
	polozky='quickget" "quickemu" "about'
	default_menu
	answer_main_menu="$answer"
}
# quickget menu
quickget_menu(){
	question='download'
	polozky="$(./quickget)"
	default_menu
	answer_download_menu="$answer"
}
# quickemu list
quickemu_list(){
	x=list
	question='run'
	polozky= +"Youparameters" -"braille"
	default_list
	answer_run_list="$answer"
}
#  menu
!CHANGE_ME!_menu(){
	question='!CHANGE_ME!'
	polozky='!CHANGE_ME!'
	default_menu
	answer_!CHANGE_ME!_menu="$answer"
}
#  list
!CHANGE_ME!_list(){
	x=list
	question='!CHANGE_ME!'
	polozky='!CHANGE_ME!'
	default_list
	answer_!CHANGE_ME!_list="$answer"
}
# answer main menu
case $answer_main_menu in
	'quickget')
		./quickget
	;;
	'quickemu')
		quickemu_list
	;;
	'about')
		>&2 echo
		about
	;;
esac
# answer 
case $answer_quickget_menu in
	'test mirrors')
		mirror_test
	;;
	'choose new mirror')
		echo choose
	;;
	'set new mirror')
		mirror_test_sort_results
	;;
	'back to main menu')
		main_menu
	;;
esac
# info
about(){
	echo info
}

main_menu
