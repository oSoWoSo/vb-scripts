#!/usr/bin/bash

# Choose best mirror
## COLORS
c1=$(tput setaf 1) # red
c2=$(tput setaf 2) # green
n=$(tput sgr0)

# required
if [ ! -f /usr/bin/fping ]; then
    echo "You are missing fping, installing..."
	sudo xbps-install -y fping
elif [ ! -f /usr/bin/fping ]; then
    echo "You are missing wget, installing..."
	sudo xbps-install -y wget
fi

DEFAULT="https://alpha.de.repo.voidlinux.org"
MIRRORLIST=$(wget -q -O- "https://raw.githubusercontent.com/void-linux/void-docs/master/src/xbps/repositories/mirrors/index.md" | grep "<http" |cut -d'<' -f2|cut -d'>' -f1 > mirrors)
old="https://alpha.de.repo.voidlinux.org"
echo "Default mirror is: ${c2}${old}${n}"
nl mirrors > list
echo ${c1}"Mirror list:"${n}

cat mirrors |cut -d'/' -f3 > pingmirrors
fping -f pingmirrors -C

cat list
read -p "Enter number of new mirror:" number
new=$(cat mirrors | awk "NR == ${number}" | tuc -d'/' -f1)
echo "Your new mirror is: ${c2}"${new}"${n}"

sudo mkdir -p /etc/xbps.d
cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d
sed -i "s|https://alpha.de.repo.voidlinux.org|${new}|g" /etc/xbps.d/*-repository-*.conf
