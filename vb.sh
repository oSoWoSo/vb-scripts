#!/usr/bin/env bash

source easybashgui
LIB_CHECK="$(type "easybashgui" 2> /dev/null )"
[ ${#LIB_CHECK} -eq 0 ] && echo -e "\n\n\n\nYou need to copy \"easybashgui\" in your path ( e.g.: \"cp easybashgui_X.X.X /usr/local/bin/\" )...\n:(\n\n\n\n" 1>&2 && exit 1
# This is only used for remove STDERR output...
exec 6>&2 ; exec 2> /dev/null
version="0.1 dev"
# disable printk
if [ -w /proc/sys/kernel/printk ]; then
    echo 0 >/proc/sys/kernel/printk
fi

version="0.1 dev"
# disable printk
if [ -w /proc/sys/kernel/printk ]; then
    echo 0 >/proc/sys/kernel/printk
fi

# functions + template INFO
# all vb templates could contain "___changeme___"
# find it all and CHANGE IT!
# Not here yet! not yet been implemented..

# ___changeme___
___changeme___(){
	echo "Not here yet!" # ___changeme___
}

# colors
Colors(){
	c0=$(tput setaf 0) # black
	c1=$(tput setaf 1) # red
	c2=$(tput setaf 2) # green
	c3=$(tput setaf 3) # yellow
	c4=$(tput setaf 4) # blue
	c5=$(tput setaf 5) # 
	c6=$(tput setaf 6) # light blue
	c7=$(tput setaf 7) # white
	c8=$(tput setaf 8) # grey
	c9=$(tput setaf 9) # 
	# reset
	c=$(tput sgr0)
}
# ISO-3166 country codes for locales
Country(){
	case "$1" in
	AD) echo "Andorra" ;;
	AE) echo "United Arab Emirates" ;;
	AL) echo "Albania" ;;
	AR) echo "Argentina" ;;
	AT) echo "Austria" ;;
	AU) echo "Australia" ;;
	BA) echo "Bonsia and Herzegovina" ;;
	BE) echo "Belgium" ;;
	BG) echo "Bulgaria" ;;
	BH) echo "Bahrain" ;;
	BO) echo "Bolivia" ;;
	BR) echo "Brazil" ;;
	BW) echo "Botswana" ;;
	BY) echo "Belarus" ;;
	CA) echo "Canada" ;;
	CH) echo "Switzerland" ;;
	CL) echo "Chile" ;;
	CN) echo "China" ;;
	CO) echo "Colombia" ;;
	CR) echo "Costa Rica" ;;
	CY) echo "Cyprus" ;;
	CZ) echo "Czech Republic" ;;
	DE) echo "Germany" ;;
	DJ) echo "Djibouti" ;;
	DK) echo "Denmark" ;;
	DO) echo "Dominican Republic" ;;
	DZ) echo "Algeria" ;;
	EC) echo "Ecuador" ;;
	EE) echo "Estonia" ;;
	EG) echo "Egypt" ;;
	ES) echo "Spain" ;;
	FI) echo "Finland" ;;
	FO) echo "Faroe Islands" ;;
	FR) echo "France" ;;
	GB) echo "Great Britain" ;;
	GE) echo "Georgia" ;;
	GL) echo "Greenland" ;;
	GR) echo "Greece" ;;
	GT) echo "Guatemala" ;;
	HK) echo "Hong Kong" ;;
	HN) echo "Honduras" ;;
	HR) echo "Croatia" ;;
	HU) echo "Hungary" ;;
	ID) echo "Indonesia" ;;
	IE) echo "Ireland" ;;
	IL) echo "Israel" ;;
	IN) echo "India" ;;
	IQ) echo "Iraq" ;;
	IS) echo "Iceland" ;;
	IT) echo "Italy" ;;
	JO) echo "Jordan" ;;
	JP) echo "Japan" ;;
	KE) echo "Kenya" ;;
	KR) echo "Korea, Republic of" ;;
	KW) echo "Kuwait" ;;
	KZ) echo "Kazakhstan" ;;
	LB) echo "Lebanon" ;;
	LT) echo "Lithuania" ;;
	LU) echo "Luxembourg" ;;
	LV) echo "Latvia" ;;
	LY) echo "Libya" ;;
	MA) echo "Morocco" ;;
	MG) echo "Madagascar" ;;
	MK) echo "Macedonia" ;;
	MT) echo "Malta" ;;
	MX) echo "Mexico" ;;
	MY) echo "Malaysia" ;;
	NI) echo "Nicaragua" ;;
	NL) echo "Netherlands" ;;
	NO) echo "Norway" ;;
	NZ) echo "New Zealand" ;;
	OM) echo "Oman" ;;
	PA) echo "Panama" ;;
	PE) echo "Peru" ;;
	PH) echo "Philippines" ;;
	PL) echo "Poland" ;;
	PR) echo "Puerto Rico" ;;
	PT) echo "Portugal" ;;
	PY) echo "Paraguay" ;;
	QA) echo "Qatar" ;;
	RO) echo "Romania" ;;
	RU) echo "Russian Federation" ;;
	SA) echo "Saudi Arabia" ;;
	SD) echo "Sudan" ;;
	SE) echo "Sweden" ;;
	SG) echo "Singapore" ;;
	SI) echo "Slovenia" ;;
	SK) echo "Slovakia" ;;
	SO) echo "Somalia" ;;
	SV) echo "El Salvador" ;;
	SY) echo "Syria" ;;
	TH) echo "Thailand" ;;
	TJ) echo "Tajikistan" ;;
	TN) echo "Tunisia" ;;
	TR) echo "Turkey" ;;
	TW) echo "Taiwan" ;;
	UA) echo "Ukraine" ;;
	UG) echo "Uganda" ;;
	US) echo "United States of America" ;;
	UY) echo "Uruguay" ;;
	UZ) echo "Uzbekistan" ;;
	VE) echo "Venezuela" ;;
	YE) echo "Yemen" ;;
	ZA) echo "South Africa" ;;
	ZW) echo "Zimbabwe" ;;
	*)  echo "$1" ;;
	esac
}

DIE(){
    rval=$1
    [ -z "$rval" ] && rval=0
    clear
    rm -f $ANSWER $TARGET_FSTAB
    # reenable printk
    if [ -w /proc/sys/kernel/printk ]; then
        echo 4 >/proc/sys/kernel/printk
    fi
    filesystems_umount
    exit $rval
}
# HELP usage
HELP(){
	SPEED_LIMIT="1048576"
	PKG_PATH="current/glibc-2.32_2.x86_64.xbps"
	cat <<_EOF | GREP_COLORS='ms=1' egrep --color "$progname|$"
----------------------------------------------------------
                          ${c1}$progname${c}
       pkg path: ${PKG_PATH}
       test speed limit: $(echo $SPEED_LIMIT | numfmt --to=iec-i --suffix=B/s)
       mirrors for testing: $(echo "$MIRRORLIST" | wc -l)
----------------------------------------------------------
 Usage: $progname -h               show this help an exit
----------------------------------------------------------
    ${c1}mirrors${c}
       $progname -m                 manually set mirror
       $progname -w [file.csv]      run speedtest and results save to file
       $progname -f file.csv        results from file pass to fzf
       $progname -r file.csv        format speedtest results from file
----------------------------------------------------------
    ${c1}install${c}
       $progname -i                 install vb to drive
----------------------------------------------------------
         ${c2}Help me make it better if you can! ${c}
----------------------------------------------------------
_EOF
}
# INFO
INFO(){
x=message
${x} "----------------------------------------------------------
                          $progname
         Install script for vb aka VoidGNU/Linux
                 and excellent Void Linux
            intended to be runned from live cd
        builded for (Ryzen 5 2600 Nvidia 1050 Ti)

                      version: $version

----------------------------------------------------------
 Usage: $progname -h             show usage help and exit
----------------------------------------------------------

          Copyright (c) 2022 zenobit
  mail: <zen@osowoso.xyz> web: https://osowoso.xyz
             licensed under MIT license

 project source: https://codeberg.org/oSoWoSo/vb-scripts

  used sources and inspirations:
  https://voidlinux.org
  https://github.com/netzverweigerer/vpm
  https://github.com/void-linux/void-mklive
  https://github.com/box-supremacy/void-installer
  https://www.reddit.com/r/voidlinux/comments/qsyk32/how_to_connect_to_the_fastest_mirrors_in_void

         Help me make it better if you can! 

       created in geany editor https://geany.org
       using easybashgui https://sites.google.com/site/easybashgui
       
----------------------------------------------------------"
}

# ISO-639 language names for locales
Language(){
	x=menu
	${x}
	case "$1" in
	aa)   echo "Afar" ;;
	af)   echo "Afrikaans" ;;
	an)   echo "Aragonese" ;;
	ar)   echo "Arabic" ;;
	ast)  echo "Asturian" ;;
	be)   echo "Belgian" ;;
	bg)   echo "Bulgarian" ;;
	bhb)  echo "Bhili" ;;
	br)   echo "Breton" ;;
	bs)   echo "Bosnian" ;;
	ca)   echo "Catalan" ;;
	cs)   echo "Czech" ;;
	cy)   echo "Welsh" ;;
	da)   echo "Danish" ;;
	de)   echo "German" ;;
	el)   echo "Greek" ;;
	en)   echo "English" ;;
	es)   echo "Spanish" ;;
	et)   echo "Estonian" ;;
	eu)   echo "Basque" ;;
	"fi") echo "Finnish" ;;
	fo)   echo "Faroese" ;;
	fr)   echo "French" ;;
	ga)   echo "Irish" ;;
	gd)   echo "Scottish Gaelic" ;;
	gl)   echo "Galician" ;;
	gv)   echo "Manx" ;;
	he)   echo "Hebrew" ;;
	hr)   echo "Croatian" ;;
	hsb)  echo "Upper Sorbian" ;;
	hu)   echo "Hungarian" ;;
	id)   echo "Indonesian" ;;
	is)   echo "Icelandic" ;;
	it)   echo "Italian" ;;
	iw)   echo "Hebrew" ;;
	ja)   echo "Japanese" ;;
	ka)   echo "Georgian" ;;
	kk)   echo "Kazakh" ;;
	kl)   echo "Kalaallisut" ;;
	ko)   echo "Korean" ;;
	ku)   echo "Kurdish" ;;
	kw)   echo "Cornish" ;;
	lg)   echo "Ganda" ;;
	lt)   echo "Lithuanian" ;;
	lv)   echo "Latvian" ;;
	mg)   echo "Malagasy" ;;
	mi)   echo "Maori" ;;
	mk)   echo "Macedonian" ;;
	ms)   echo "Malay" ;;
	mt)   echo "Maltese" ;;
	nb)   echo "Norwegian Bokmål" ;;
	nl)   echo "Dutch" ;;
	nn)   echo "Norwegian Nynorsk" ;;
	oc)   echo "Occitan" ;;
	om)   echo "Oromo" ;;
	pl)   echo "Polish" ;;
	pt)   echo "Portugese" ;;
	ro)   echo "Romanian" ;;
	ru)   echo "Russian" ;;
	sk)   echo "Slovak" ;;
	sl)   echo "Slovenian" ;;
	so)   echo "Somali" ;;
	sq)   echo "Albanian" ;;
	st)   echo "Southern Sotho" ;;
	sv)   echo "Swedish" ;;
	tcy)  echo "Tulu" ;;
	tg)   echo "Tajik" ;;
	th)   echo "Thai" ;;
	tl)   echo "Tagalog" ;;
	tr)   echo "Turkish" ;;
	uk)   echo "Ukrainian" ;;
	uz)   echo "Uzbek" ;;
	wa)   echo "Walloon" ;;
	xh)   echo "Xhosa" ;;
	yi)   echo "Yiddish" ;;
	zh)   echo "Chinese" ;;
	zu)   echo "Zulu" ;;
	*)    echo "$1" ;;
	esac
}
# detect if this is an EFI system.
check_efi(){
	if [ -e /sys/firmware/efi/systab ]; then
		EFI_SYSTEM=1
		echo -e "\n${c2} EFI system enabled ${c}"
		EFI_FW_BITS=$(cat /sys/firmware/efi/fw_platform_size)
		if [ $EFI_FW_BITS -eq 32 ]; then
			EFI_TARGET=i386-efi
		else
			EFI_TARGET=x86_64-efi
		fi
	else
		echo -e "\n${c1} no EFI system support ${c}\n"
		echo -e "${c2}exiting...${c}"
		exit 1
	fi
}

check_file(){
	if [ -e /usr/bin/spacefm ]; then
	echo "___changeme___"
	fi
}

# rootcheck
check_root(){
	if [[ $EUID -gt 0 ]]; then
		echo -e "\n${c1}This operation needs super-user privileges."
		echo -e "${c2}exiting...${c}"
		exit 255
	fi
}
# done
completed(){
	echo "${c2}done${c}"
}

configure_net(){
    local dev="$1" rval

    DIALOG --yesno "Do you want to use DHCP for $dev?" ${YESNOSIZE}
    rval=$?
    if [ $rval -eq 0 ]; then
        configure_net_dhcp $dev
    elif [ $rval -eq 1 ]; then
        configure_net_static $dev
    fi
}

configure_net_dhcp(){
    local dev="$1"

    iface_setup $dev
    if [ $? -eq 1 ]; then
        dhcpcd -t 10 -w -4 -L $dev -e "wpa_supplicant_conf=/etc/wpa_supplicant/wpa_supplicant-${dev}.conf" 2>&1 | tee $LOG | \
            DIALOG --progressbox "Initializing $dev via DHCP..." ${WIDGET_SIZE}
        if [ $? -ne 0 ]; then
            DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} failed to run dhcpcd. See $LOG for details." ${MSGBOXSIZE}
            return 1
        fi
        iface_setup $dev
        if [ $? -eq 1 ]; then
            DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} DHCP request failed for $dev. Check $LOG for errors." ${MSGBOXSIZE}
            return 1
        fi
    fi
    test_network
    if [ $? -eq 1 ]; then
        set_option NETWORK "${dev} dhcp"
    fi
}

configure_net_static(){
    local ip gw dns1 dns2 dev=$1

    DIALOG --form "Static IP configuration for $dev:" 0 0 0 \
        "IP address:" 1 1 "192.168.0.2" 1 21 20 0 \
        "Gateway:" 2 1 "192.168.0.1" 2 21 20 0 \
        "DNS Primary" 3 1 "8.8.8.8" 3 21 20 0 \
        "DNS Secondary" 4 1 "8.8.4.4" 4 21 20 0 || return 1

    set -- $(cat $ANSWER)
    ip=$1; gw=$2; dns1=$3; dns2=$4
    echo "running: ip link set dev $dev up" >$LOG
    ip link set dev $dev up >$LOG 2>&1
    if [ $? -ne 0 ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} Failed to bring $dev interface." ${MSGBOXSIZE}
        return 1
    fi
    echo "running: ip addr add $ip dev $dev" >$LOG
    ip addr add $ip dev $dev >$LOG 2>&1
    if [ $? -ne 0 ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} Failed to set ip to the $dev interface." ${MSGBOXSIZE}
        return 1
    fi
    ip route add default via $gw >$LOG 2>&1
    if [ $? -ne 0 ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} failed to setup your gateway." ${MSGBOXSIZE}
        return 1
    fi
    echo "nameserver $dns1" >/etc/resolv.conf
    echo "nameserver $dns2" >>/etc/resolv.conf
    test_network
    if [ $? -eq 1 ]; then
        set_option NETWORK "${dev} static $ip $gw $dns1 $dns2"
    fi
}

configure_wifi(){
    local dev="$1" ssid enc pass _wpasupconf=/etc/wpa_supplicant/wpa_supplicant.conf

    DIALOG --form "Wireless configuration for ${dev}\n(encryption type: wep or wpa)" 0 0 0 \
        "SSID:" 1 1 "" 1 16 30 0 \
        "Encryption:" 2 1 "" 2 16 4 3 \
        "Password:" 3 1 "" 3 16 63 0 || return 1
    set -- $(cat $ANSWER)
    ssid="$1"; enc="$2"; pass="$3";

    if [ -z "$ssid" ]; then
        DIALOG --msgbox "Invalid SSID." ${MSGBOXSIZE}
        return 1
    elif [ -z "$enc" -o "$enc" != "wep" -a "$enc" != "wpa" ]; then
        DIALOG --msgbox "Invalid encryption type (possible values: wep or wpa)." ${MSGBOXSIZE}
        return 1
    elif [ -z "$pass" ]; then
        DIALOG --msgbox "Invalid AP password." ${MSGBOXSIZE}
    fi

    rm -f ${_wpasupconf%.conf}-${dev}.conf
    cp -f ${_wpasupconf} ${_wpasupconf%.conf}-${dev}.conf
    if [ "$enc" = "wep" ]; then
        echo "network={" >> ${_wpasupconf%.conf}-${dev}.conf
        echo "  ssid=\"$ssid\"" >> ${_wpasupconf%.conf}-${dev}.conf
        echo "  wep_key0=\"$pass\"" >> ${_wpasupconf%.conf}-${dev}.conf
        echo "  wep_tx_keyidx=0" >> ${_wpasupconf%.conf}-${dev}.conf
        echo "  auth_alg=SHARED" >> ${_wpasupconf%.conf}-${dev}.conf
        echo "}" >> ${_wpasupconf%.conf}-${dev}.conf
    else
        wpa_passphrase "$ssid" "$pass" >> ${_wpasupconf%.conf}-${dev}.conf
    fi

    configure_net_dhcp $dev
    return $?
}

copy_rootfs(){
    local tar_in="--create --one-file-system --xattrs"
    TITLE="Check $LOG for details ..."
    INFOBOX "Counting files, please be patient ..." 4 60
    copy_total=$(tar ${tar_in} -v -f /dev/null / 2>/dev/null | wc -l)
    export copy_total copy_count=0 copy_progress=
    clear
    tar ${tar_in} -f - / 2>/dev/null | \
        tar --extract --xattrs --xattrs-include='*' --preserve-permissions -v -f - -C $TARGETDIR | \
        log_and_count | \
        DIALOG --title "${TITLE}" \
            --progressbox "Copying live image to target rootfs." 5 60
    if [ $? -ne 0 ]; then
        DIE 1
    fi
    unset copy_total copy_count copy_percent
}

enable_dhcpd(){
    ln -sf /etc/sv/dhcpcd $TARGETDIR/etc/runit/runsvdir/default/dhcpcd
}

filesystems_validate(){
    local mnts dev size fstype mntpt mkfs rootfound fmt
    local usrfound efi_system_partition
    local bootdev=$(get_option BOOTLOADER)

    unset TARGETFS
    mnts=$(grep -E '^MOUNTPOINT.*' $CONF_FILE)
    set -- ${mnts}
    while [ $# -ne 0 ]; do
        fmt=""
        dev=$2; fstype=$3; size=$4; mntpt="$5"; mkfs=$6
        shift 6

        if [ "$mntpt" = "/" ]; then
            rootfound=1
        elif [ "$mntpt" = "/usr" ]; then
            usrfound=1
        elif [ "$fstype" = "vfat" -a "$mntpt" = "/boot/efi" ]; then
            efi_system_partition=1
        fi
        if [ "$mkfs" -eq 1 ]; then
            fmt="NEW FILESYSTEM: "
        fi
        if [ -z "$TARGETFS" ]; then
            TARGETFS="${fmt}$dev ($size) mounted on $mntpt as ${fstype}\n"
        else
            TARGETFS="${TARGETFS}${fmt}${dev} ($size) mounted on $mntpt as ${fstype}\n"
        fi
    done
    if [ -z "$rootfound" ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
the mount point for the root filesystem (/) has not yet been configured." ${MSGBOXSIZE}
        return 1
    elif [ -n "$usrfound" ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
/usr mount point has been configured but is not supported, please remove it to continue." ${MSGBOXSIZE}
        return 1
    elif [ -n "$EFI_SYSTEM" -a "$bootdev" != "none" -a -z "$efi_system_partition" ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
The EFI System Partition has not yet been configured, please create it\n
as FAT32, mountpoint /boot/efi and at least with 100MB of size." ${MSGBOXSIZE}
        return 1
    fi
    FILESYSTEMS_DONE=1
}

filesystems_create(){
    local mnts dev mntpt fstype fspassno mkfs size rv uuid

    mnts=$(grep -E '^MOUNTPOINT.*' $CONF_FILE)
    set -- ${mnts}
    while [ $# -ne 0 ]; do
        dev=$2; fstype=$3; mntpt="$5"; mkfs=$6
        shift 6

        # swap partitions
        if [ "$fstype" = "swap" ]; then
            swapoff $dev >/dev/null 2>&1
            if [ "$mkfs" -eq 1 ]; then
                mkswap $dev >$LOG 2>&1
                if [ $? -ne 0 ]; then
                    DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
failed to create swap on ${dev}!\ncheck $LOG for errors." ${MSGBOXSIZE}
                    DIE 1
                fi
            fi
            swapon $dev >$LOG 2>&1
            if [ $? -ne 0 ]; then
                DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
failed to activate swap on $dev!\ncheck $LOG for errors." ${MSGBOXSIZE}
                DIE 1
            fi
            # Add entry for target fstab
            uuid=$(blkid -o value -s UUID "$dev")
            echo "UUID=$uuid none swap sw 0 0" >>$TARGET_FSTAB
            continue
        fi

        if [ "$mkfs" -eq 1 ]; then
            case "$fstype" in
            btrfs) MKFS="mkfs.btrfs -f"; modprobe btrfs >$LOG 2>&1;;
            ext2) MKFS="mke2fs -F"; modprobe ext2 >$LOG 2>&1;;
            ext3) MKFS="mke2fs -F -j"; modprobe ext3 >$LOG 2>&1;;
            ext4) MKFS="mke2fs -F -t ext4"; modprobe ext4 >$LOG 2>&1;;
            f2fs) MKFS="mkfs.f2fs -f"; modprobe f2fs >$LOG 2>&1;;
            vfat) MKFS="mkfs.vfat -F32"; modprobe vfat >$LOG 2>&1;;
            xfs) MKFS="mkfs.xfs -f -i sparse=0"; modprobe xfs >$LOG 2>&1;;
            esac
            TITLE="Check $LOG for details ..."
            INFOBOX "Creating filesystem $fstype on $dev for $mntpt ..." 8 60
            echo "Running $MKFS $dev..." >$LOG
            $MKFS $dev >$LOG 2>&1; rv=$?
            if [ $rv -ne 0 ]; then
                DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
failed to create filesystem $fstype on $dev!\ncheck $LOG for errors." ${MSGBOXSIZE}
                DIE 1
            fi
        fi
        # Mount rootfs the first one.
        [ "$mntpt" != "/" ] && continue
        mkdir -p $TARGETDIR
        echo "Mounting $dev on $mntpt ($fstype)..." >$LOG
        mount -t $fstype $dev $TARGETDIR >$LOG 2>&1
        if [ $? -ne 0 ]; then
            DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
failed to mount $dev on ${mntpt}! check $LOG for errors." ${MSGBOXSIZE}
            DIE 1
        fi
        # Add entry to target fstab
        uuid=$(blkid -o value -s UUID "$dev")
        if [ "$fstype" = "f2fs" ]; then
            fspassno=0
        else
            fspassno=1
        fi
        echo "UUID=$uuid $mntpt $fstype defaults 0 $fspassno" >>$TARGET_FSTAB
    done

    # mount all filesystems in target rootfs
    mnts=$(grep -E '^MOUNTPOINT.*' $CONF_FILE)
    set -- ${mnts}
    while [ $# -ne 0 ]; do
        dev=$2; fstype=$3; mntpt="$5"
        shift 6
        [ "$mntpt" = "/" -o "$fstype" = "swap" ] && continue
        mkdir -p ${TARGETDIR}${mntpt}
        echo "Mounting $dev on $mntpt ($fstype)..." >$LOG
        mount -t $fstype $dev ${TARGETDIR}${mntpt} >$LOG 2>&1
        if [ $? -ne 0 ]; then
            DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
failed to mount $dev on $mntpt! check $LOG for errors." ${MSGBOXSIZE}
            DIE
        fi
        # Add entry to target fstab
        uuid=$(blkid -o value -s UUID "$dev")
        echo "UUID=$uuid $mntpt $fstype defaults 0 2" >>$TARGET_FSTAB
    done
}

filesystems_mount(){
    for f in sys proc dev; do
        [ ! -d $TARGETDIR/$f ] && mkdir $TARGETDIR/$f
        echo "Mounting $TARGETDIR/$f..." >$LOG
        mount --rbind /$f $TARGETDIR/$f >$LOG 2>&1
    done
}

filesystems_umount(){
    local f

    for f in sys/fs/fuse/connections sys proc dev; do
        echo "Unmounting $TARGETDIR/$f..." >$LOG
        umount $TARGETDIR/$f >$LOG 2>&1
    done
    local mnts="$(grep -E '^MOUNTPOINT.*$' $CONF_FILE)"
    set -- ${mnts}
    while [ $# -ne 0 ]; do
        local dev=$2; local fstype=$3; local mntpt=$5
        shift 6
        if [ "$fstype" = "swap" ]; then
            echo "Disabling swap space on $dev..." >$LOG
            swapoff $dev >$LOG 2>&1
            continue
        fi
        if [ "$mntpt" != "/" ]; then
            echo "Unmounting $TARGETDIR/$mntpt..." >$LOG
            umount $TARGETDIR/$mntpt >$LOG 2>&1
        fi
    done
    echo "Unmounting $TARGETDIR..." >$LOG
    umount $TARGETDIR >$LOG 2>&1
}

# format drives
format(){
	echo -e "\n${c4}Formating partitions${c1}...${c}"
	sudo mkfs.vfat /dev/${boot}
	completed
	sudo mkfs.btrfs -f /dev/${root}
	completed
}

get_option(){
    echo $(grep -E "^${1}.*" $CONF_FILE|sed -e "s|${1}||")
}
# ISO-639 language names for locales
Language(){
	case "$1" in
	aa)  echo "Afar" ;;
	af)  echo "Afrikaans" ;;
	an)  echo "Aragonese" ;;
	ar)  echo "Arabic" ;;
	ast) echo "Asturian" ;;
	be)  echo "Belgian" ;;
	bg)  echo "Bulgarian" ;;
	bhb) echo "Bhili" ;;
	br)  echo "Breton" ;;
	bs)  echo "Bosnian" ;;
	ca)  echo "Catalan" ;;
	cs)  echo "Czech" ;;
	cy)  echo "Welsh" ;;
	da)  echo "Danish" ;;
	de)  echo "German" ;;
	el)  echo "Greek" ;;
	en)  echo "English" ;;
	es)  echo "Spanish" ;;
	et)  echo "Estonian" ;;
	eu)  echo "Basque" ;;
  "fi")  echo "Finnish" ;;
	fo)  echo "Faroese" ;;
	fr)  echo "French" ;;
	ga)  echo "Irish" ;;
	gd)  echo "Scottish Gaelic" ;;
	gl)  echo "Galician" ;;
	gv)  echo "Manx" ;;
	he)  echo "Hebrew" ;;
	hr)  echo "Croatian" ;;
	hsb) echo "Upper Sorbian" ;;
	hu)  echo "Hungarian" ;;
	id)  echo "Indonesian" ;;
	is)  echo "Icelandic" ;;
	it)  echo "Italian" ;;
	iw)  echo "Hebrew" ;;
	ja)  echo "Japanese" ;;
	ka)  echo "Georgian" ;;
	kk)  echo "Kazakh" ;;
	kl)  echo "Kalaallisut" ;;
	ko)  echo "Korean" ;;
	ku)  echo "Kurdish" ;;
	kw)  echo "Cornish" ;;
	lg)  echo "Ganda" ;;
	lt)  echo "Lithuanian" ;;
	lv)  echo "Latvian" ;;
	mg)  echo "Malagasy" ;;
	mi)  echo "Maori" ;;
	mk)  echo "Macedonian" ;;
	ms)  echo "Malay" ;;
	mt)  echo "Maltese" ;;
	nb)  echo "Norwegian Bokmål" ;;
	nl)  echo "Dutch" ;;
	nn)  echo "Norwegian Nynorsk" ;;
	oc)  echo "Occitan" ;;
	om)  echo "Oromo" ;;
	pl)  echo "Polish" ;;
	pt)  echo "Portugese" ;;
	ro)  echo "Romanian" ;;
	ru)  echo "Russian" ;;
	sk)  echo "Slovak" ;;
	sl)  echo "Slovenian" ;;
	so)  echo "Somali" ;;
	sq)  echo "Albanian" ;;
	st)  echo "Southern Sotho" ;;
	sv)  echo "Swedish" ;;
	tcy) echo "Tulu" ;;
	tg)  echo "Tajik" ;;
	th)  echo "Thai" ;;
	tl)  echo "Tagalog" ;;
	tr)  echo "Turkish" ;;
	uk)  echo "Ukrainian" ;;
	uz)  echo "Uzbek" ;;
	wa)  echo "Walloon" ;;
	xh)  echo "Xhosa" ;;
	yi)  echo "Yiddish" ;;
	zh)  echo "Chinese" ;;
	zu)  echo "Zulu" ;;
	*)   echo "$1" ;;
	esac
}
# detect if this is an EFI system.
check_efi(){
	if [ -e /sys/firmware/efi/systab ]; then
		EFI_SYSTEM=1
		echo -e "\n${c2} EFI system enabled ${c}"
		EFI_FW_BITS=$(cat /sys/firmware/efi/fw_platform_size)
		if [ $EFI_FW_BITS -eq 32 ]; then
			EFI_TARGET=i386-efi
		else
			EFI_TARGET=x86_64-efi
		fi
	else
		echo -e "\n${c1} no EFI system support ${c}\n"
		echo -e "${c2}exiting...${c}"
		exit 1
	fi
}

check_file(){
	if [ -e /usr/bin/spacefm ]; then
	echo "___changeme___"
	fi
}

# rootcheck
check_root(){
	if [[ $EUID -gt 0 ]]; then
		echo -e "\n${c1}This operation needs super-user privileges."
		echo -e "${c2}exiting...${c}"
		exit 255
	fi
}
# done
completed(){
	echo "${c2}done${c}"
}

configure_net(){
    local dev="$1" rval

    DIALOG --yesno "Do you want to use DHCP for $dev?" ${YESNOSIZE}
    rval=$?
    if [ $rval -eq 0 ]; then
        configure_net_dhcp $dev
    elif [ $rval -eq 1 ]; then
        configure_net_static $dev
    fi
}

configure_net_dhcp(){
    local dev="$1"

    iface_setup $dev
    if [ $? -eq 1 ]; then
        dhcpcd -t 10 -w -4 -L $dev -e "wpa_supplicant_conf=/etc/wpa_supplicant/wpa_supplicant-${dev}.conf" 2>&1 | tee $LOG | \
            DIALOG --progressbox "Initializing $dev via DHCP..." ${WIDGET_SIZE}
        if [ $? -ne 0 ]; then
            DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} failed to run dhcpcd. See $LOG for details." ${MSGBOXSIZE}
            return 1
        fi
        iface_setup $dev
        if [ $? -eq 1 ]; then
            DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} DHCP request failed for $dev. Check $LOG for errors." ${MSGBOXSIZE}
            return 1
        fi
    fi
    test_network
    if [ $? -eq 1 ]; then
        set_option NETWORK "${dev} dhcp"
    fi
}

configure_net_static(){
    local ip gw dns1 dns2 dev=$1

    DIALOG --form "Static IP configuration for $dev:" 0 0 0 \
        "IP address:" 1 1 "192.168.0.2" 1 21 20 0 \
        "Gateway:" 2 1 "192.168.0.1" 2 21 20 0 \
        "DNS Primary" 3 1 "8.8.8.8" 3 21 20 0 \
        "DNS Secondary" 4 1 "8.8.4.4" 4 21 20 0 || return 1

    set -- $(cat $ANSWER)
    ip=$1; gw=$2; dns1=$3; dns2=$4
    echo "running: ip link set dev $dev up" >$LOG
    ip link set dev $dev up >$LOG 2>&1
    if [ $? -ne 0 ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} Failed to bring $dev interface." ${MSGBOXSIZE}
        return 1
    fi
    echo "running: ip addr add $ip dev $dev" >$LOG
    ip addr add $ip dev $dev >$LOG 2>&1
    if [ $? -ne 0 ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} Failed to set ip to the $dev interface." ${MSGBOXSIZE}
        return 1
    fi
    ip route add default via $gw >$LOG 2>&1
    if [ $? -ne 0 ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} failed to setup your gateway." ${MSGBOXSIZE}
        return 1
    fi
    echo "nameserver $dns1" >/etc/resolv.conf
    echo "nameserver $dns2" >>/etc/resolv.conf
    test_network
    if [ $? -eq 1 ]; then
        set_option NETWORK "${dev} static $ip $gw $dns1 $dns2"
    fi
}

configure_wifi(){
    local dev="$1" ssid enc pass _wpasupconf=/etc/wpa_supplicant/wpa_supplicant.conf

    DIALOG --form "Wireless configuration for ${dev}\n(encryption type: wep or wpa)" 0 0 0 \
        "SSID:" 1 1 "" 1 16 30 0 \
        "Encryption:" 2 1 "" 2 16 4 3 \
        "Password:" 3 1 "" 3 16 63 0 || return 1
    set -- $(cat $ANSWER)
    ssid="$1"; enc="$2"; pass="$3";

    if [ -z "$ssid" ]; then
        DIALOG --msgbox "Invalid SSID." ${MSGBOXSIZE}
        return 1
    elif [ -z "$enc" -o "$enc" != "wep" -a "$enc" != "wpa" ]; then
        DIALOG --msgbox "Invalid encryption type (possible values: wep or wpa)." ${MSGBOXSIZE}
        return 1
    elif [ -z "$pass" ]; then
        DIALOG --msgbox "Invalid AP password." ${MSGBOXSIZE}
    fi

    rm -f ${_wpasupconf%.conf}-${dev}.conf
    cp -f ${_wpasupconf} ${_wpasupconf%.conf}-${dev}.conf
    if [ "$enc" = "wep" ]; then
        echo "network={" >> ${_wpasupconf%.conf}-${dev}.conf
        echo "  ssid=\"$ssid\"" >> ${_wpasupconf%.conf}-${dev}.conf
        echo "  wep_key0=\"$pass\"" >> ${_wpasupconf%.conf}-${dev}.conf
        echo "  wep_tx_keyidx=0" >> ${_wpasupconf%.conf}-${dev}.conf
        echo "  auth_alg=SHARED" >> ${_wpasupconf%.conf}-${dev}.conf
        echo "}" >> ${_wpasupconf%.conf}-${dev}.conf
    else
        wpa_passphrase "$ssid" "$pass" >> ${_wpasupconf%.conf}-${dev}.conf
    fi

    configure_net_dhcp $dev
    return $?
}

copy_rootfs(){
    local tar_in="--create --one-file-system --xattrs"
    TITLE="Check $LOG for details ..."
    INFOBOX "Counting files, please be patient ..." 4 60
    copy_total=$(tar ${tar_in} -v -f /dev/null / 2>/dev/null | wc -l)
    export copy_total copy_count=0 copy_progress=
    clear
    tar ${tar_in} -f - / 2>/dev/null | \
        tar --extract --xattrs --xattrs-include='*' --preserve-permissions -v -f - -C $TARGETDIR | \
        log_and_count | \
        DIALOG --title "${TITLE}" \
            --progressbox "Copying live image to target rootfs." 5 60
    if [ $? -ne 0 ]; then
        DIE 1
    fi
    unset copy_total copy_count copy_percent
}

enable_dhcpd(){
    ln -sf /etc/sv/dhcpcd $TARGETDIR/etc/runit/runsvdir/default/dhcpcd
}

filesystems_validate(){
    local mnts dev size fstype mntpt mkfs rootfound fmt
    local usrfound efi_system_partition
    local bootdev=$(get_option BOOTLOADER)

    unset TARGETFS
    mnts=$(grep -E '^MOUNTPOINT.*' $CONF_FILE)
    set -- ${mnts}
    while [ $# -ne 0 ]; do
        fmt=""
        dev=$2; fstype=$3; size=$4; mntpt="$5"; mkfs=$6
        shift 6

        if [ "$mntpt" = "/" ]; then
            rootfound=1
        elif [ "$mntpt" = "/usr" ]; then
            usrfound=1
        elif [ "$fstype" = "vfat" -a "$mntpt" = "/boot/efi" ]; then
            efi_system_partition=1
        fi
        if [ "$mkfs" -eq 1 ]; then
            fmt="NEW FILESYSTEM: "
        fi
        if [ -z "$TARGETFS" ]; then
            TARGETFS="${fmt}$dev ($size) mounted on $mntpt as ${fstype}\n"
        else
            TARGETFS="${TARGETFS}${fmt}${dev} ($size) mounted on $mntpt as ${fstype}\n"
        fi
    done
    if [ -z "$rootfound" ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
the mount point for the root filesystem (/) has not yet been configured." ${MSGBOXSIZE}
        return 1
    elif [ -n "$usrfound" ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
/usr mount point has been configured but is not supported, please remove it to continue." ${MSGBOXSIZE}
        return 1
    elif [ -n "$EFI_SYSTEM" -a "$bootdev" != "none" -a -z "$efi_system_partition" ]; then
        DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
The EFI System Partition has not yet been configured, please create it\n
as FAT32, mountpoint /boot/efi and at least with 100MB of size." ${MSGBOXSIZE}
        return 1
    fi
    FILESYSTEMS_DONE=1
}

filesystems_create(){
    local mnts dev mntpt fstype fspassno mkfs size rv uuid

    mnts=$(grep -E '^MOUNTPOINT.*' $CONF_FILE)
    set -- ${mnts}
    while [ $# -ne 0 ]; do
        dev=$2; fstype=$3; mntpt="$5"; mkfs=$6
        shift 6

        # swap partitions
        if [ "$fstype" = "swap" ]; then
            swapoff $dev >/dev/null 2>&1
            if [ "$mkfs" -eq 1 ]; then
                mkswap $dev >$LOG 2>&1
                if [ $? -ne 0 ]; then
                    DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
failed to create swap on ${dev}!\ncheck $LOG for errors." ${MSGBOXSIZE}
                    DIE 1
                fi
            fi
            swapon $dev >$LOG 2>&1
            if [ $? -ne 0 ]; then
                DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
failed to activate swap on $dev!\ncheck $LOG for errors." ${MSGBOXSIZE}
                DIE 1
            fi
            # Add entry for target fstab
            uuid=$(blkid -o value -s UUID "$dev")
            echo "UUID=$uuid none swap sw 0 0" >>$TARGET_FSTAB
            continue
        fi

        if [ "$mkfs" -eq 1 ]; then
            case "$fstype" in
            btrfs) MKFS="mkfs.btrfs -f"; modprobe btrfs >$LOG 2>&1;;
            ext2) MKFS="mke2fs -F"; modprobe ext2 >$LOG 2>&1;;
            ext3) MKFS="mke2fs -F -j"; modprobe ext3 >$LOG 2>&1;;
            ext4) MKFS="mke2fs -F -t ext4"; modprobe ext4 >$LOG 2>&1;;
            f2fs) MKFS="mkfs.f2fs -f"; modprobe f2fs >$LOG 2>&1;;
            vfat) MKFS="mkfs.vfat -F32"; modprobe vfat >$LOG 2>&1;;
            xfs) MKFS="mkfs.xfs -f -i sparse=0"; modprobe xfs >$LOG 2>&1;;
            esac
            TITLE="Check $LOG for details ..."
            INFOBOX "Creating filesystem $fstype on $dev for $mntpt ..." 8 60
            echo "Running $MKFS $dev..." >$LOG
            $MKFS $dev >$LOG 2>&1; rv=$?
            if [ $rv -ne 0 ]; then
                DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
failed to create filesystem $fstype on $dev!\ncheck $LOG for errors." ${MSGBOXSIZE}
                DIE 1
            fi
        fi
        # Mount rootfs the first one.
        [ "$mntpt" != "/" ] && continue
        mkdir -p $TARGETDIR
        echo "Mounting $dev on $mntpt ($fstype)..." >$LOG
        mount -t $fstype $dev $TARGETDIR >$LOG 2>&1
        if [ $? -ne 0 ]; then
            DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
failed to mount $dev on ${mntpt}! check $LOG for errors." ${MSGBOXSIZE}
            DIE 1
        fi
        # Add entry to target fstab
        uuid=$(blkid -o value -s UUID "$dev")
        if [ "$fstype" = "f2fs" ]; then
            fspassno=0
        else
            fspassno=1
        fi
        echo "UUID=$uuid $mntpt $fstype defaults 0 $fspassno" >>$TARGET_FSTAB
    done

    # mount all filesystems in target rootfs
    mnts=$(grep -E '^MOUNTPOINT.*' $CONF_FILE)
    set -- ${mnts}
    while [ $# -ne 0 ]; do
        dev=$2; fstype=$3; mntpt="$5"
        shift 6
        [ "$mntpt" = "/" -o "$fstype" = "swap" ] && continue
        mkdir -p ${TARGETDIR}${mntpt}
        echo "Mounting $dev on $mntpt ($fstype)..." >$LOG
        mount -t $fstype $dev ${TARGETDIR}${mntpt} >$LOG 2>&1
        if [ $? -ne 0 ]; then
            DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
failed to mount $dev on $mntpt! check $LOG for errors." ${MSGBOXSIZE}
            DIE
        fi
        # Add entry to target fstab
        uuid=$(blkid -o value -s UUID "$dev")
        echo "UUID=$uuid $mntpt $fstype defaults 0 2" >>$TARGET_FSTAB
    done
}

filesystems_mount(){
    for f in sys proc dev; do
        [ ! -d $TARGETDIR/$f ] && mkdir $TARGETDIR/$f
        echo "Mounting $TARGETDIR/$f..." >$LOG
        mount --rbind /$f $TARGETDIR/$f >$LOG 2>&1
    done
}

filesystems_umount(){
    local f

    for f in sys/fs/fuse/connections sys proc dev; do
        echo "Unmounting $TARGETDIR/$f..." >$LOG
        umount $TARGETDIR/$f >$LOG 2>&1
    done
    local mnts="$(grep -E '^MOUNTPOINT.*$' $CONF_FILE)"
    set -- ${mnts}
    while [ $# -ne 0 ]; do
        local dev=$2; local fstype=$3; local mntpt=$5
        shift 6
        if [ "$fstype" = "swap" ]; then
            echo "Disabling swap space on $dev..." >$LOG
            swapoff $dev >$LOG 2>&1
            continue
        fi
        if [ "$mntpt" != "/" ]; then
            echo "Unmounting $TARGETDIR/$mntpt..." >$LOG
            umount $TARGETDIR/$mntpt >$LOG 2>&1
        fi
    done
    echo "Unmounting $TARGETDIR..." >$LOG
    umount $TARGETDIR >$LOG 2>&1
}

# format drives
format(){
	echo -e "\n${c4}Formating partitions${c1}...${c}"
	mkfs.vfat /dev/${boot}
	completed
	mkfs.btrfs -f /dev/${root}
	completed
}

get_option(){
    echo $(grep -E "^${1}.*" $CONF_FILE|sed -e "s|${1}||")
}

iface_setup(){
    ip addr show dev $1|grep -q 'inet '
    return $?
}
# install vb to drive
install() {
echo -e "\n${c2}-${c3}-${c1}- ${c2}Let's ${c3}begin ${c1}installing ${c2}-${c3}-${c1}-${n}"
# run functions
install_required
#check_root
check_efi
umount_all

iface_setup(){
    ip addr show dev $1|grep -q 'inet '
    return $?
}
# install vb to drive
install(){

	variables
	echo -e "\n${c2}-${c3}-${c1}- ${c2}Let's ${c3}begin ${c1}installing ${c2}-${c3}-${c1}-${c}"
	TARGETDIR=/mnt/target
	LOG=/dev/tty8
	CONF_FILE=.vb.conf
	if [ ! -f $CONF_FILE ]; then
		touch -f $CONF_FILE
	fi
	ANSWER=$(mktemp -t vinstall-XXXXXXXX || exit 1)
	TARGET_FSTAB=$(mktemp -t vinstall-fstab-XXXXXXXX || exit 1)
	trap "DIE" INT TERM QUIT

	install_required
	check_efi
	umount_all
	echo -e "${c4}Mounting cache${c1}...${c}"
	sudo mount /dev/nvme0n1p4 /var/cache/
	echo -e "\n${c1}Required${c} two partitions (${c1}boot${c} and ${c1}root${c})"
	echo -e "- ${c3}recommended${c} separate home partition"
	echo -e "- ${c3}optional${c} separate cache partition"

	use_gparted
	sleep 1

	next
	echo -e "${c1}Drives list:${c}"
	sudo fdisk -x | grep "/dev/"

	set_boot
	set_root
	set_home
	set_cache
	format
	mount_target
	install_method
	echo -e "\n${c2}Entering chroot${c}"
	echo -e "${c4}Remounting cache${c1}...${c}"
	sudo cp -r /mnt/target/var/cache/ /var/
	sudo umount /dev/${cache}
	sudo mount /dev/${cache} /mnt/target/var/cache

	mount_chroot
	echo -e "${c4}Copying the DNS configuration${c1}...${c}"
	sudo cp /etc/resolv.conf /mnt/target/etc/
	echo -e "${c4}Copying setted mirrors${c1}...${c}"
	sudo cp -r /etc/xbps.d /mnt/target/etc/
	echo -e "${c4}Copying installation script to chroot${c1}...${c}"
	sudo cp vb.sh /mnt/target
	echo -e "\n${c2}Chroot into the new installation${c}"
	PS1='(chroot) # ' sudo chroot /mnt/target/ /bin/bash  "./vb2.sh"
}
# install awesome WM
install_awesome(){
	sudo xbps-install\
	 awesome\
	 setxkbmap
	cp -r /etc/xdg/awesome ~/.config/awesome
	setxkbmap -layout "us,cz" -option "grp:shifts_toggle"
	completed
}
# install bspwm
install_bspwm(){
	sudo xbps-install\
	 bspwm\
	 dbus\
	 dmenu\
	 elogind\
	 lxappearance\
	 lxsession\
	 sxhkd\
	 tint2\
	 yambar
	mkdir ~/.config/bspwm
	cp /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
	chmod +x ~/.config/bspwm/bspwmrc
	mkdir ~/.config/sxhkd
	cp /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc
	completed
}

# installation method
install_method (){
	echo -e "\n${c2}Choose installation method...${c}"
	echo -e "Possible (type in number):
	- 1 rootfs
	- 2 xbps ${c3}recommended${c}
	- 3 cd copy"
	read -p "" method
	case $method in
		1 )
			xbps-install -y xz
			echo -e "${c4}Downloading void linux ROOTFS tarball${c1}...${c}"
			## Detect if ROOTFS already downloaded.
			if [ -e ./void-ROOTFS.tar.xz ]; then
				tar xvf void-ROOTFS.tar.xz -C /mnt/target
			else
				curl https://mirror.fit.cvut.cz/voidlinux/live/current/void-x86_64-ROOTFS-20210930.tar.xz -o void-ROOTFS.tar.xz
				tar xvf void-ROOTFS.tar.xz -C /mnt/target
			fi
		;;
	
		2 )
			REPO=${newrepo}/current
			ARCH=x86_64
			echo -e "${c4}Installing base system${c1}...${c}"
			XBPS_ARCH=$ARCH xbps-install -Sy -r /mnt/target -R "$REPO" base-system fish-shell refind
			;;
		3 )
			echo "Not implemented yet!"
		;;
	esac
}
# install openbox WM
install_openbox(){
	echo "Not here yet!" # ___changeme___
	
}

install_packages(){
    local _grub= _syspkg=

    if [ -n "$EFI_SYSTEM" ]; then
        if [ $EFI_FW_BITS -eq 32 ]; then
            _grub="grub-i386-efi"
        else
            _grub="grub-x86_64-efi"
        fi
    else
        _grub="grub"
    fi

    _syspkg="base-system"

    mkdir -p $TARGETDIR/var/db/xbps/keys $TARGETDIR/usr/share
    cp -a /usr/share/xbps.d $TARGETDIR/usr/share/
    cp /var/db/xbps/keys/*.plist $TARGETDIR/var/db/xbps/keys
    mkdir -p $TARGETDIR/boot/grub

    _arch=$(xbps-uhelper arch)

    stdbuf -oL env XBPS_ARCH=${_arch} \
        xbps-install  -r $TARGETDIR -SyU ${_syspkg} ${_grub} 2>&1 | \
        DIALOG --title "Installing base system packages..." \
        --programbox 24 80
    if [ $? -ne 0 ]; then
        DIE 1
    fi
    xbps-reconfigure -r $TARGETDIR -f base-files >/dev/null 2>&1
    chroot $TARGETDIR xbps-reconfigure -a
}

install_quickemu(){
	curl https://github.com/quickemu-project/quickemu -o quickget
	curl https://github.com/quickemu-project/quickemu -o quickemu
	curl https://github.com/quickemu-project/quickemu -o quickemu
}
# required dependencies
install_required(){
	sudo xbps-install -y xtools gparted fzf mtools wget
}

log_and_count(){
    local progress whole tenth
    while read line; do
        echo "$line" >$LOG
        copy_count=$((copy_count + 1))
        progress=$((1000 * copy_count / copy_total))
        if [ "$progress" != "$copy_progress" ]; then
            whole=$((progress / 10))
            tenth=$((progress % 10))
            printf "Progress: %d.%d%% (%d of %d files)\n" $whole $tenth $copy_count $copy_total
            copy_progress=$progress
        fi
    done
}

# LVM
lvm(){
	echo "Not here yet!" # CHANGE IT!
	
}
# mirror defaults
mirror(){
	SPEED_LIMIT="1048576"
	MIRRORLIST="$(wget -q -O- "https://raw.githubusercontent.com/void-linux/void-docs/master/src/xbps/repositories/mirrors/index.md" | grep "<http" |cut -d'<' -f2 |cut -d'>' -f1)"
	PKG_PATH="current/glibc-2.32_2.x86_64.xbps"
}
# mirror speed limit
mirror_speed(){
	mirror
	curl -Y $SPEED_LIMIT --progress-bar $1 -o/dev/null --write-out "%{speed_download}"
}
# test mirrors
mirror_test(){
	mirror
	count=1
	echo "$MIRRORLIST" | while read -r mirror etc; do
		pkg_url="${mirror}${PKG_PATH}"
		info=$(echo $etc | sed 's/,//g')
		>&2 echo "${count}. $pkg_url"
		echo "${mirror},${info},$(time_appconnect $pkg_url),$(mirror_speed $pkg_url)"
		>&2 echo
		count=$((count + 1))
	done
}
# sort mirrors test results
mirror_test_sort_results(){
	mirror
	cat "$1" | sort -t, -nrk4 | while IFS=, read url info time_appconnect mirror_speed; do
		f_time=$(printf "%.2fs" $time_appconnect)
		f_speed=$(echo $mirror_speed | numfmt --to=iec-i --suffix=B/s)
		echo "${url},${info},${f_time},${f_speed}"
	done | column -s, -t
}
# mount chroot
mount_chroot(){
	sudo mount --rbind /sys /mnt/target/sys && sudo mount --make-rslave /mnt/target/sys
	sudo mount --rbind /dev /mnt/target/dev && sudo mount --make-rslave /mnt/target/dev
	sudo mount --rbind /proc /mnt/target/proc && sudo mount --make-rslave /mnt/target/proc
}
# mount target
mount_target(){
	echo -e "${c4}Mounting partitions${c1}...${c}"
	sudo mkdir -p /mnt/target
	sudo mount /dev/${root} /mnt/target/
	sudo mkdir -p /mnt/target/boot
	sudo mount /dev/${boot} /mnt/target/boot
	sudo mkdir -p /mnt/target/home
	sudo mount /dev/${home} /mnt/target/home/
	sudo mkdir -p /mnt/target/var/cache
	sudo mount /dev/${cache} /mnt/target/var/cache/
}
# continue installing
next(){
	echo "Wanna continue? y/N"
	read -p "" gonext
	case $gonext in
	y) ;;
	
	*) exit 1 ;;
	
	esac
}
# run as user
runasuser(){
	if [ "$(id -u)" != "0" ]; then
	   echo "run as user" 1>&2
	fi
}
# run as root
runasroot(){
	if [ "$(id -u)" = "0" ]; then
	   echo "run as root" 1>&2
	fi
}
# set boot partition
set_boot(){
	echo -e "${c2}Enter partition for boot: ${c1}required${c}"
	echo -e "(${c4}blue${c} = personal defaults)"
	echo -e "(sda1 ${c4}sdb1${c} vda1 nvme0n1p1${c1}...${c})"
	read -p "" boot
	echo -e "${c4}root will be on ${c1}${boot}${c}"
	export boot
}
# set bootloader
set_bootloader(){
	local dev=$(get_option BOOTLOADER) grub_args=

	if [ "$dev" = "none" ]; then return; fi

	# Check if it's an EFI system via efivars module.
	if [ -n "$EFI_SYSTEM" ]; then
		grub_args="--target=$EFI_TARGET --efi-directory=/boot/efi --bootloader-id=void_grub --recheck"
	fi
	echo "Running grub-install $grub_args $dev..." >$LOG
	chroot $TARGETDIR grub-install $grub_args $dev >$LOG 2>&1
	if [ $? -ne 0 ]; then
		DIALOG --msgbox "${BOLD}${RED}ERROR:${RESET} \
		failed to install GRUB to $dev!\nCheck $LOG for errors." ${MSGBOXSIZE}
		DIE 1
	fi
	echo "Running grub-mkconfig on $TARGETDIR..." >$LOG
	chroot $TARGETDIR grub-mkconfig -o /boot/grub/grub.cfg >$LOG 2>&1
	if [ $? -ne 0 ]; then
		DIALOG --msgbox "${BOLD}${RED}ERROR${RESET}: \
		failed to run grub-mkconfig!\nCheck $LOG for errors." ${MSGBOXSIZE}
		DIE 1
	fi
}
# set cache partition
set_cache(){
	echo -e "\n${c2}Enter partition for cache: ${c3}optional${c}"
	echo -e "(sda4 sdb4 vda4 ${c4}nvme0n1p4${c}${c1}...${c})"
	read -p "" cache
	echo -e "${c4}root will be on ${c1}${cache}${c}"
	export cache
	sudo umount /dev/$CACHE
	sudo mount /dev/$CACHE /var/cache/
	completed
}
# set home partition
set_home(){
	echo -e "\n${c2}Enter partition for home: ${c3}recommended${c}"
	echo -e "(sda3 sdb3 vda3 ${c4}nvme0n1p3${c}${c1}...${c})"
	read -p "" home
	echo -e "${c4}root will be on ${c1}${home}${c}"
	export home
}
# set hostname
set_hostname(){
	echo $(get_option HOSTNAME) > $TARGETDIR/etc/hostname
}
# set keymap
set_keymap(){
	local KEYMAP=$(get_option KEYMAP)
	if [ -f /etc/vconsole.conf ]; then
		sed -i -e "s|KEYMAP=.*|KEYMAP=$KEYMAP|g" $TARGETDIR/etc/vconsole.conf
	else
		sed -i -e "s|#\?KEYMAP=.*|KEYMAP=$KEYMAP|g" $TARGETDIR/etc/rc.conf
	fi
}
# set locales
set_locale(){
	if [ -f $TARGETDIR/etc/default/libc-locales ]; then
		local LOCALE=$(get_option LOCALE)
		sed -i -e "s|LANG=.*|LANG=$LOCALE|g" $TARGETDIR/etc/locale.conf
		# Uncomment locale from /etc/default/libc-locales and regenerate it.
		sed -e "/${LOCALE}/s/^\#//" -i $TARGETDIR/etc/default/libc-locales
		echo "Running xbps-reconfigure -f glibc-locales ..." >$LOG
		chroot $TARGETDIR xbps-reconfigure -f glibc-locales >$LOG 2>&1
	fi
}
# set new mirror
set_mirror(){
	mirror
	echo -e "${c2}Changing mirrors...${c}"
	defaultrepo="https://alpha.de.repo.voidlinux.org"
	echo "default repository: ${defaultrepo}"
	newrepo="https://mirror.fit.cvut.cz/voidlinux"
	echo "new repository: ${newrepo}"
	sudo mkdir -p /etc/xbps.d
	sudo cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
	sudo sed -i "s|https://alpha.de.repo.voidlinux.org|$newrepo|g" /etc/xbps.d/*-repository-*.conf
	echo -e "${c4}Synchronizing${c1}...${c}"
	sudo xbps-install -Suvy
}
# set options
set_option(){
	if grep -Eq "^${1}.*" $CONF_FILE; then
		sed -i -e "/^${1}.*/d" $CONF_FILE
	fi
	echo "${1} ${2}" >>$CONF_FILE
}
# set progname
set_progname(){
	progname=${progname:="${0##*/}"}
}
# set options
set_option() {
	if grep -Eq "^${1}.*" $CONF_FILE; then
		sed -i -e "/^${1}.*/d" $CONF_FILE
	fi
	echo "${1} ${2}" >>$CONF_FILE
}
# set progname
set_progname(){
	progname=${progname:="${0##*/}"}
}
# set root partition
set_root(){
	echo -e "\n${c2}Enter partition for root: ${c1}required${c}"
	echo -e "(sda2 ${c4}sdb2${c} vda2 nvme0n1p2${c1}...${c})"
	read -p "" root
	echo -e "${c4}root will be on ${c1}${root}${c}"
	export root
}
# set root password
set_rootpassword(){
	echo "root:$(get_option ROOTPASSWORD)" | chpasswd -R $TARGETDIR -c SHA512
}
# set user shell
set_shell(){
	sudo chsh /usr/bin/$SHELL
}
# set timezone
set_timezone(){
	local TIMEZONE="$(get_option TIMEZONE)"

	sed -i -e "s|#TIMEZONE=.*|TIMEZONE=$TIMEZONE|g" $TARGETDIR/etc/rc.conf
}
# set user account
set_useraccount(){
	[ -z "$USERLOGIN_DONE" ] && return
	[ -z "$USERPASSWORD_DONE" ] && return
	[ -z "$USERNAME_DONE" ] && return
	[ -z "$USERGROUPS_DONE" ] && return
	useradd -R $TARGETDIR -m -G $(get_option USERGROUPS) \
		-c "$(get_option USERNAME)" $(get_option USERLOGIN)
	echo "$(get_option USERLOGIN):$(get_option USERPASSWORD)" | \
		chpasswd -R $TARGETDIR -c SHA512
}
# show disks
show_disks(){
	local dev size sectorsize gbytes

	# IDE
	for dev in $(ls /sys/block|grep -E '^hd'); do
		if [ "$(cat /sys/block/$dev/device/media)" = "disk" ]; then
			# Find out nr sectors and bytes per sector;
			echo "/dev/$dev"
			size=$(cat /sys/block/$dev/size)
			sectorsize=$(cat /sys/block/$dev/queue/hw_sector_size)
			gbytes="$(($size * $sectorsize / 1024 / 1024 / 1024))"
			echo "size:${gbytes}GB;sector_size:$sectorsize"
		fi
	done
	# SATA/SCSI and Virtual disks (virtio)
	for dev in $(ls /sys/block|grep -E '^([sv]|xv)d|mmcblk|nvme'); do
		echo "/dev/$dev"
		size=$(cat /sys/block/$dev/size)
		sectorsize=$(cat /sys/block/$dev/queue/hw_sector_size)
		gbytes="$(($size * $sectorsize / 1024 / 1024 / 1024))"
		echo "size:${gbytes}GB;sector_size:$sectorsize"
	done
	# cciss(4) devices
	for dev in $(ls /dev/cciss 2>/dev/null|grep -E 'c[0-9]d[0-9]$'); do
		echo "/dev/cciss/$dev"
		size=$(cat /sys/block/cciss\!$dev/size)
		sectorsize=$(cat /sys/block/cciss\!$dev/queue/hw_sector_size)
		gbytes="$(($size * $sectorsize / 1024 / 1024 / 1024))"
		echo "size:${gbytes}GB;sector_size:$sectorsize"
	done
}
# show partitions
show_partitions(){
	local dev fstype fssize p part

	set -- $(show_disks)
	while [ $# -ne 0 ]; do
		disk=$(basename $1)
		shift 2
		# ATA/SCSI/SATA
		for p in /sys/block/$disk/$disk*; do
			if [ -d $p ]; then
				part=$(basename $p)
				fstype=$(lsblk -nfr /dev/$part|awk '{print $2}'|head -1)
				[ "$fstype" = "iso9660" ] && continue
				[ "$fstype" = "crypto_LUKS" ] && continue
				[ "$fstype" = "LVM2_member" ] && continue
				fssize=$(lsblk -nr /dev/$part|awk '{print $4}'|head -1)
				echo "/dev/$part"
				echo "size:${fssize:-unknown};fstype:${fstype:-none}"
			fi
		done
	done
	# Device Mapper
	for p in /dev/mapper/*; do
		part=$(basename $p)
		[ "${part}" = "live-rw" ] && continue
		[ "${part}" = "live-base" ] && continue
		[ "${part}" = "control" ] && continue

		fstype=$(lsblk -nfr $p|awk '{print $2}'|head -1)
		fssize=$(lsblk -nr $p|awk '{print $4}'|head -1)
		echo "${p}"
		echo "size:${fssize:-unknown};fstype:${fstype:-none}"
	done
	# Software raid (md)
	for p in $(ls -d /dev/md* 2>/dev/null|grep '[0-9]'); do
		part=$(basename $p)
		if cat /proc/mdstat|grep -qw $part; then
			fstype=$(lsblk -nfr /dev/$part|awk '{print $2}')
			[ "$fstype" = "crypto_LUKS" ] && continue
			[ "$fstype" = "LVM2_member" ] && continue
			fssize=$(lsblk -nr /dev/$part|awk '{print $4}')
			echo "$p"
			echo "size:${fssize:-unknown};fstype:${fstype:-none}"
		fi
	done
	# cciss(4) devices
	for part in $(ls /dev/cciss 2>/dev/null|grep -E 'c[0-9]d[0-9]p[0-9]+'); do
		fstype=$(lsblk -nfr /dev/cciss/$part|awk '{print $2}')
		[ "$fstype" = "crypto_LUKS" ] && continue
		[ "$fstype" = "LVM2_member" ] && continue
		fssize=$(lsblk -nr /dev/cciss/$part|awk '{print $4}')
		echo "/dev/cciss/$part"
		echo "size:${fssize:-unknown};fstype:${fstype:-none}"
	done
	if [ -e /sbin/lvs ]; then
		# LVM
		lvs --noheadings|while read lvname vgname perms size; do
			echo "/dev/mapper/${vgname}-${lvname}"
			echo "size:${size};fstype:lvm"
		done
	fi
}

test_network(){
    rm -f xtraeme.asc && \
        xbps-uhelper fetch http://alpha.de.repo.voidlinux.org/live/xtraeme.asc >$LOG 2>&1
    if [ $? -eq 0 ]; then
        DIALOG --msgbox "Network is working properly!" ${MSGBOXSIZE}
        NETWORK_DONE=1
        return 1
    fi
    DIALOG --msgbox "Network is inaccessible, please set it up properly." ${MSGBOXSIZE}
}
# curl timeout
time_appconnect(){
	curl --connect-timeout 2 -o/dev/null -sw '%{time_appconnect}' -I $1
}
# unmount all partitions
umount_all(){
	echo -e "\n${c4}Unmounting all partitions${c1}...${c}"
	sudo umount /dev/sda1
	sudo umount /dev/sda2
	sudo umount /dev/sda3
	sudo umount /dev/sda4
	sudo umount /dev/sdb1
	sudo umount /dev/sdb2
	sudo umount /dev/sdb3
	sudo umount /dev/sdb4
	sudo umount /dev/vda1
	sudo umount /dev/vda2
	sudo umount /dev/vda3
	sudo umount /dev/vda4
	sudo umount /dev/vdb1
	sudo umount /dev/vdb2
	sudo umount /dev/vdb3
	sudo umount /dev/vdb4
	sudo umount /dev/nvme0n1p1
	sudo umount /dev/nvme0n1p2
	sudo umount /dev/nvme0n1p3
	sudo umount /dev/nvme0n1p4
}
# update system
update(){
	sudo xbps-install -Sy xbps
	sudo xbps-install -Syu
	xcheckrestart
}
# prepare drives for installation
use_gparted(){
	echo -e "${c2}Use gparted...?${c}" && read gpart
	case $gpart in
		y )
			sudo gparted &
		;;
		* )
		;;
	esac
}
# Make sure we don't inherit these from environment
variables(){
	SOURCE_DONE=
	HOSTNAME_DONE=
	KEYBOARD_DONE=
	LOCALE_DONE=
	TIMEZONE_DONE=
	ROOTPASSWORD_DONE=
	USERLOGIN_DONE=
	USERPASSWORD_DONE=
	USERNAME_DONE=
	USERGROUPS_DONE=
	BOOTLOADER_DONE=
	PARTITIONS_DONE=
	NETWORK_DONE=
	FILESYSTEMS_DONE=
}
# exit
quit(){
	echo "----------------------------------------------------------"
	echo "       Enjoy and help me make it better please!"
	echo "       https://osowoso.xyx     <pm@osowoso.xyz>"
	echo "----------------------------------------------------------"
	exit 0
}
# RUN
Colors
set_progname

default_menu(){
	x=menu
		{
		QUESTION="$question"
		x="menu -w 650 -h 550"
		${x} "$polozky"
		ANSWER="$(cat "${dir_tmp}/${file_tmp}" )"
		}
}

main_menu(){
	question='Main menu'
	polozky='test mirrors" "choose new mirror" "set new mirror" "install vb to drive" "other installation'
	default_menu
	answer_main_menu="$ANSWER"
}
main_menu
echo "$answer_main_menu"


	#-f) mirror_test_sort_results $2 | fzf +s;;
	#-i) echo "installing vb to drive" && install;;
	#-m) echo "Choose mirror:" && set_mirror;;
	#-r) mirror_test_sort_results $2;;
	#-w) [ -z $2 ] && { mirror_test; exit 0; } || { mirror_test >"$2"; exit 0; };;
	#-h) HELP;;
	#*) INFO;;

# Exit immediately if a command exits with a non-zero exit status
#set -e
exit 0
