#!/usr/bin/env bash
#

if [ ! -f /usr/bin/easybashgui ]; then
	wget https://github.com/BashGui/easybashgui/archive/refs/tags/12.0.4.tar.gz -o 12.0.4.tar.gz && tar -xf 12.0.4.tar.gz
fi

export supertitle="vb installer" ; source easybashgui

LIB_CHECK="$(type "easybashgui" 2> /dev/null )"
[ ${#LIB_CHECK} -eq 0 ] && echo -e "\n\n\n\nYou need to copy \"easybashgui\" in your path ( e.g.: \"cp easybashgui_X.X.X /usr/local/bin/\" )...\n:(\n\n\n\n" 1>&2 && exit 1

# This is only used for remove STDERR output...
#exec 6>&2 ; exec 2> /dev/null
#

version="0.1 dev"
progname=${progname:="${0##*/}"}

# ... "x" is the function name...
#

if [ ! -f /usr/bin/fping ]; then
x=alert_message
${x} "                    You are missing required fping


                              Press >OK< for install

                                 Cancel for exit"
sudo xbps-install -y fping
fi

if [ ! -f /usr/bin/wget ]; then
x=alert_message
${x} "                    You are missing required wget


                              Press >OK< for install

                                 Cancel for exit"
sudo xbps-install -y wget
fi

# default menu
default_menu(){
	x=tagged_menu
		{
		QUESTION="$question"
		x="tagged_menu -w 400 -h 400"
		${x} "$polozky"
		answer="$(cat "${dir_tmp}/${file_tmp}" )"
		}
}

answer()
	{
	answer="$(cat "${dir_tmp}/${file_tmp}" )"
	}

# info
info(){
	x=message
	${x} "----------------------------------------------------------------------------------------------------------
                                                   $progname
                                      Install script for vb aka Void GNU/Linux
                                             and excellent Void Linux
                                      intended to be runned from live cd
                                 builded for (Ryzen 5 2600 Nvidia 1050 Ti)

                                                 version: $version

                                   Copyright (c) 2022 zenobit
                           mail: <zen@osowoso.xyz> web: https://osowoso.xyz
                                      licensed under MIT license

                          project source: https://codeberg.org/oSoWoSo/vb-scripts

                           used sources and inspirations:
                           https://voidlinux.org
                           https://github.com/netzverweigerer/vpm
                           https://github.com/void-linux/void-mklive
                           https://github.com/box-supremacy/void-installer
                           https://www.reddit.com/r/voidlinux

                                  Help me make it better if you can! 

                                created in geany editor https://geany.org
                     using easybashgui https://sites.google.com/site/easybashgui

----------------------------------------------------------------------------------------------------------"
}

list1()
	{
	x=list
		{
		message "Choose whatever you want..."
		list +"choose best mirror" +"install vb" +"install software" +"exit" ; answer
		answer_list1="$answer"
		export answer_list1
		echo "$answer_list1"
		case $answer_list1 in
			'choose best mirror')
			mirrors
			;;
	
			'install vb')
				install_vb
			;;
	
			'software software')
				install_software
			;;
	
			'exit')
				exit
			;;
		esac
		}
	}

info
list1
