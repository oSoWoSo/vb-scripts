#!/usr/bin/env bash

export supertitle="vb installer" ; source easybashgui
LIB_CHECK="$(type "easybashgui" 2> /dev/null )"
[ ${#LIB_CHECK} -eq 0 ] && echo -e "\n\n\n\nYou need to copy \"easybashgui\" in your path ( e.g.: \"cp easybashgui_X.X.X /usr/local/bin/\" )...\n:(\n\n\n\n" 1>&2 && exit 1
# This is only used for remove STDERR output...
exec 6>&2 ; exec 2> /dev/null
#exec 6>&2 ; exec 2>1
version="0.1 dev"
progname=${progname:="${0##*/}"}

# Make sure we don't inherit these from environment
	SOURCE_DONE=
	HOSTNAME_DONE=
	KEYBOARD_DONE=
	LOCALE_DONE=
	TIMEZONE_DONE=
	ROOTPASSWORD_DONE=
	USERLOGIN_DONE=
	USERPASSWORD_DONE=
	USERNAME_DONE=
	USERGROUPS_DONE=
	BOOTLOADER_DONE=
	PARTITIONS_DONE=
	NETWORK_DONE=
	FILESYSTEMS_DONE=
# required
	if [ ! -f /usr/bin/fping ]; then
		x=alert_message
		${x} "                    You are missing required fping
		
		
		                              Press >OK< for install
		
		                                 Cancel for exit"
		sudo xbps-install -y fping
	fi
	if [ ! -f /usr/bin/wget ]; then
		x=alert_message
		${x} "                    You are missing required wget
		
		
		                              Press >OK< for install
		
		                                 Cancel for exit"
		sudo xbps-install -y wget
	fi
# default menu
default_menu(){
	x=menu
		{
		QUESTION="$question"
		x="menu -w 400 -h 400"
		${x} "$polozky"
		answer="$(cat "${dir_tmp}/${file_tmp}" )"
		}
}
# default list
default_list(){
	x=list
		{
		QUESTION="$question"
		x="list -w 400 -h 400"
		list "$polozky"
		answer="$(cat "${dir_tmp}/${file_tmp}" )"
		}
}
# main menu
main_menu(){
	question='Main menu'
	polozky='set mirror" "revert mirror" "install" "other" "info'
	default_menu
	answer_main_menu="$answer"
}
#  menu
_menu(){
	question=''
	polozky=''
	default_menu
	answer__menu="$answer"
}
# info
info(){
x=message
${x} "----------------------------------------------------------------------------------------------------------
                                                   $progname
                                      Install script for vb aka Void GNU/Linux
                                             and excellent Void Linux
                                      intended to be runned from live cd
                                 builded for (Ryzen 5 2600 Nvidia 1050 Ti)

                                                 version: $version

                                   Copyright (c) 2022 zenobit
                           mail: <zen@osowoso.xyz> web: https://osowoso.xyz
                                      licensed under MIT license

                          project source: https://codeberg.org/oSoWoSo/vb-scripts

                           used sources and inspirations:
                           https://voidlinux.org
                           https://github.com/netzverweigerer/vpm
                           https://github.com/void-linux/void-mklive
                           https://github.com/box-supremacy/void-installer
                           https://www.reddit.com/r/voidlinux

                                  Help me make it better if you can! 

                                created in geany editor https://geany.org
                     using easybashgui https://sites.google.com/site/easybashgui

----------------------------------------------------------------------------------------------------------"
}

# install vb aka void GNU/Linux
install(){
	echo -e "\n${c2}-${c3}-${c1}- ${c2}Let's ${c3}begin ${c1}installing ${c2}-${c3}-${c1}-${c}"
	TARGETDIR=/mnt/target
	LOG=/dev/tty8
	CONF_FILE=.vb.conf
	if [ ! -f $CONF_FILE ]; then
		touch -f $CONF_FILE
	fi
	ANSWER=$(mktemp -t vinstall-XXXXXXXX || exit 1)
	TARGET_FSTAB=$(mktemp -t vinstall-fstab-XXXXXXXX || exit 1)
	trap "DIE" INT TERM QUIT

	install_required
	check_efi
	umount_all
	echo -e "${c4}Mounting cache${c1}...${c}"
	sudo mount /dev/nvme0n1p4 /var/cache/
	echo -e "\n${c1}Required${c} two partitions (${c1}boot${c} and ${c1}root${c})"
	echo -e "- ${c3}recommended${c} separate home partition"
	echo -e "- ${c3}optional${c} separate cache partition"

	use_gparted
	sleep 1

	next
	echo -e "${c1}Drives list:${c}"
	sudo fdisk -x | grep "/dev/"

	set_boot
	set_root
	set_home
	set_cache
	format
	mount_target
	install_method
	echo -e "\n${c2}Entering chroot${c}"
	echo -e "${c4}Remounting cache${c1}...${c}"
	sudo cp -r /mnt/target/var/cache/ /var/
	sudo umount /dev/${cache}
	sudo mount /dev/${cache} /mnt/target/var/cache

	mount_chroot
	echo -e "${c4}Copying the DNS configuration${c1}...${c}"
	sudo cp /etc/resolv.conf /mnt/target/etc/
	echo -e "${c4}Copying setted mirrors${c1}...${c}"
	sudo cp -r /etc/xbps.d /mnt/target/etc/
	echo -e "${c4}Copying installation script to chroot${c1}...${c}"
	sudo cp vb.sh /mnt/target
	echo -e "\n${c2}Chroot into the new installation${c}"
	PS1='(chroot) # ' sudo chroot /mnt/target/ /bin/bash  "./vb2.sh"
}
install_vb(){
	set_useraccount
}
# mirror defaults
mirrors(){
	SPEED_LIMIT="1048576"
	MIRRORLIST="$(wget -q -O- "https://raw.githubusercontent.com/void-linux/void-docs/master/src/xbps/repositories/mirrors/index.md" | grep "<http" |cut -d'<' -f2 |cut -d'>' -f1)"
	PKG_PATH="current/glibc-2.32_2.x86_64.xbps"
}
# mirror speed limit
mirror_speed(){
	mirrors
	curl -Y $SPEED_LIMIT --progress-bar $1 -o/dev/null --write-out "%{speed_download}"
}
# test mirrors
mirror_test(){
	mirror_speed
	count=1
	echo "$MIRRORLIST" | while read -r mirrors etc; do
		pkg_url="${mirror}${PKG_PATH}"
		info=$(echo $etc | sed 's/,//g')
		>&2 echo "${count}. $pkg_url"
		echo "${mirror},${info},$(time_appconnect $pkg_url),$(mirror_speed $pkg_url)"
		>&2 echo
		count=$((count + 1))
	done
}
# sort mirrors test results
mirror_test_sort_results(){
	mirror_test
	cat "$1" | sort -t, -nrk4 | while IFS=, read url info time_appconnect mirror_speed; do
		f_time=$(printf "%.2fs" $time_appconnect)
		f_speed=$(echo $mirror_speed | numfmt --to=iec-i --suffix=B/s)
		echo "${url},${info},${f_time},${f_speed}"
	done | column -s, -t
}
# set mirror
set_mirror(){
	mirror_test_sort_results
	echo "Current ${c2}$(cat /etc/xbps.d/00-repository-main.conf)${n}"
	nl mirrors > list
	echo ${c1}"Mirror list:"${n}
	cat mirrors |cut -d'/' -f3 > pingmirrors
	fping -f pingmirrors -C
	cat list
	read -p "Enter number of new mirror:" number
	new=$(cat mirrors | awk "NR == ${number}" | tuc -d'/' -f1)
	echo "Your new mirror is: ${c2}${new}${n}"
	sudo mkdir -p /etc/xbps.d
	sudo cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d
	sudo sed -i "s|https://alpha.de.repo.voidlinux.org|${new}|g" /etc/xbps.d/*-repository-*.conf
}
# set back to default mirror
setback_mirror(){
	sudo cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d
}

# set user
set_useraccount(){
	x=input
	{
	${x} 3 "hostname" "(enter hostname)" "username" "(enter username)" "password" "(enter password)"
	
	}
}

# RUN

x=info
	{
	while :
		do
		${x} "----------------------------------------------------------------------------------------------------------
                                                   $progname
                                      Install script for vb aka Void GNU/Linux
                                             and excellent Void Linux
                                      intended to be runned from live cd
                                 builded for (Ryzen 5 2600 Nvidia 1050 Ti)

                                                 version: $version

                                   Copyright (c) 2022 zenobit
                           mail: <zen@osowoso.xyz> web: https://osowoso.xyz
                                      licensed under MIT license

                          project source: https://codeberg.org/oSoWoSo/vb-scripts

                           used sources and inspirations:
                           https://voidlinux.org
                           https://github.com/netzverweigerer/vpm
                           https://github.com/void-linux/void-mklive
                           https://github.com/box-supremacy/void-installer
                           https://www.reddit.com/r/voidlinux

                                  Help me make it better if you can! 

                                created in geany editor https://geany.org
                     using easybashgui https://sites.google.com/site/easybashgui

----------------------------------------------------------------------------------------------------------
                                 click \"Ok\" for continue ..."
		answer="${?}"
		if [ ${answer} -eq 0 ]
			then
			break
		elif [ ${answer} -eq 1 ]
			then
			exit
		else
			exit
		fi
	done
	}

main_menu

case $answer_main_menu in
	'set mirror')
		exec ./mirror.sh -f test
	;;

	'install')
		install_vb
	;;

	'other')
		echo other
	;;

	'info')
		>&2 echo
		info
	;;

esac
