# vb-scripts

  [void linux](https://voidlinux.org)

# What is void linux?

  you don't know?

###  you can try online

 1. open [distrotest](https://distrotest.net/VoidLinux)

 2. choose desired desktop and click >start<

# vb aka GNU/Linux installer and manager

dependencies: `bash` , `curl` and `mtools`

optional: `fzf` , `gparted` and `xtools`(xcheckrestart)


### If you want to contribute, fork this repository...

 or from terminal:
  
  `git clone https://codeberg.org/oSoWoSo/vb-scripts`

### If you want try vb aka GNU/Linux or vb installator/manager
  
  in terminal:

  `wget https://codeberg.org/oSoWoSo/vb-scripts/raw/branch/main/vb.sh && chmod +x vb.sh`
  
  or
  
  `curl https://codeberg.org/oSoWoSo/vb-scripts/raw/branch/main/vb.sh -o vb.sh && chmod +x vb.sh`

  run
  
  `./vb.sh`

  Enjoy!

  Help me make it better if you can!